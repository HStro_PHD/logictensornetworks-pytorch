import torch.nn as nn


class CustomParameterList(nn.ParameterList):
    def __init__(self, parameters=[], parameter_names=[]):
        super(CustomParameterList, self).__init__(parameters)
        self.ordered_names = parameter_names
