import torch
import torch.nn.functional as F

from .utils import cross_args, cross_2args

# Add cross entropy option for universal aggregation
# Use the bilinear layer = positive definite on each slice trick
# Make sure any initialised embeddings don't allow the sigmoid to end up to high
# or low in output, as gradients will be small

class SyntaxBackend(object):

    TNORM = 'luk'
    UNIVERSAL_AGGREG='hmean'
    EXISTENTIAL_AGGREG='max'

    #######################
    # Connective Backends #
    #######################
    @classmethod
    def F_And(cls, wffs):
        if cls.TNORM == 'min':
            return torch.min(wffs, dim=-1, keepdim=True)

        elif cls.TNORM == 'prod':
            return torch.prod(wffs, dim=-1, keepdim=True)

        elif cls.TNORM == 'mean':
            return torch.mean(wffs, dim=-1, keepdim=True)

        elif cls.TNORM == 'luk':
            # Check if sum of groundings (which would ideally all be 1) are
            # greater than the number of instances. The plus 1 means if all ground
            # to 1 the output is 1, meaning AND returns perfect truth.
            return torch.max(
                    torch.zeros((1,), dtype=torch.float32),
                    torch.sum(wffs, dim=-1, keepdim=True) + 1 - wffs.shape[-1]
                    )

    @classmethod
    def F_Or(cls, wffs):
        if cls.TNORM == 'min':
            return torch.max(wffs, dim=-1, keepdim=True)

        elif cls.TNORM == 'prod':
            return 1 - torch.prod(1 - wffs, dim=-1, keepdim=True)

        elif cls.TNORM == 'mean':
            return 1 - torch.max(1 - wffs, dim=-1, keepdim=True)

        elif cls.TNORM == 'luk':
            return 1 - torch.min(
                    torch.sum(wffs, dim=-1, keepdim=True), torch.tensor(1.))

    @classmethod
    def F_Implies(cls, wff1, wff2):
        if cls.TNORM == 'min':
            # Elementwise max comparison - final tensor has all max values
            return torch.max(torch.le(wff1, wff2), wff2)

        elif cls.TNORM == 'prod':
            # Elementwise max comparison - final tensor has all max values
            le_wff1_wff2 = torch.le(wff1, wff2)
            gt_wff1_wff2 = torch.gt(wff1, wff2)
            if torch.equal(wff1[0], 0):
                return le_wff1_wff2 + gt_wff1_wff2*wff2/wff1
            else:
                return torch.tensor([1.])

        elif cls.TNORM == 'mean':
            return torch.clamp(2*wff2-wff1, min=0, max=1)

        elif cls.TNORM == 'luk':
            return torch.min(torch.tensor(1.), 1 - wff1 + wff2)

    @classmethod
    def F_Not(cls, wff):
        return 1 - wff

    @classmethod
    def F_Equiv(cls, wff1, wff2):
        if cls.TNORM == 'min':
            return torch.max(torch.equal(wff1, wff2), torch.min(wff1, wff2))

        elif cls.TNORM == 'prod':
            return torch.min(wff1/wff2, wff2/wff1)

        elif cls.TNORM == 'mean':
            return 1 - torch.abs(wff1-wff2)

        elif cls.TNORM == 'luk':
            return 1 - torch.abs(wff1-wff2)

    ################
    # Quantifiers  #
    ################
    @classmethod
    def F_Forall(cls, wff):
        if cls.UNIVERSAL_AGGREG == "hmean":
            return 1/torch.mean(1/(wff+1e-10))#, dim=0)

        elif cls.UNIVERSAL_AGGREG == "min":
            return torch.min(wff, dim=0)[0]  #TODO Not sure why this is needed. Returns tuple..

        elif cls.UNIVERSAL_AGGREG == "mean":
            return torch.mean(wff, dim=0)

        elif cls.UNIVERSAL_AGGREG == "xentropy":
            #target = torch.ones_like(wff, dtype=torch.float)
            #return #F.binary_cross_entropy(wff, target).unsqueeze(-1)
            return (torch.log(wff).mean().unsqueeze(0))

    @classmethod
    def F_Exists(cls, wff):
        if cls.EXISTENTIAL_AGGREG == "max":
            return torch.max(wff, dim=0)[0]

#########################
# Connective Frontends  #
#########################
class And(object):

    def __init__(self, *wffs):
        self.wffs = wffs

    def __call__(self):
        if len(self.wffs) == 0:
            result = torch.tensor(0.)
        else:
            arg_results = [wff() for wff in self.wffs]
            cross_wffs, _ = cross_args(arg_results)
            result =  SyntaxBackend.F_And(cross_wffs)#.clone()
        return result

class Or(object):

    def __init__(self, *wffs):
        self.wffs = wffs

    def __call__(self):
        if len(self.wffs) == 0:
            result = torch.tensor(0.)
        else:
            arg_results = [wff() for wff in self.wffs]
            cross_wffs, _ = cross_args(arg_results)
            result = SyntaxBackend.F_Or(cross_wffs)#.clone()
        return result

class Implies(object):

    def __init__(self, wff1, wff2):
        self.wff1 = wff1
        self.wff2 = wff2

    def __call__(self):
        _, cross_wffs = cross_2args(self.wff1(), self.wff2())
        arg1_results = cross_wffs[0]
        arg2_results = cross_wffs[1]
        result = SyntaxBackend.F_Implies(arg1_results, arg2_results)
        #result = result.clone()
        return result


class Not(object):

    def __init__(self, wff):
        self.wff = wff

    def __call__(self):
        arg1_results = self.wff()
        result = SyntaxBackend.F_Not(arg1_results)
        #result = result.clone()
        return result

class Equiv(object):

    def __init__(self, wff1, wff2):
        self.wff1 = wff1
        self.wff2 = wff2

    def __call__(self):
        _, cross_wffs = cross_2args(self.wff1(), self.wff2())
        arg1_results = cross_wffs[0]
        arg2_results = cross_wffs[1]
        result = SyntaxBackend.F_Equiv(arg1_results, arg2_results)
        #result = result.clone()
        return result

class Forall(object):
    """
    Needs to take in a sentence, which is a function that should be called with
    its variables during train. Before, calling with variables 'preps' machinery
    to point at the right variable class, which acts as a dataset holder.
    Somehow the Forall clause should be defined with the underlying functions and variables
    provided, but so that it can just be 'called' and all the underlying functions
    get called too, but in order (bottom up so each above one has real values to acces)

    Basically, each piece needs to call its arguments if its arguments are callable.
    If in initialisation, they should get armed so that they point to the right things when called
    """
    # TODO currently variables aren't used for anything.
    def __init__(self, variables, sentence):
        self.sentence = sentence
        if type(variables) is not tuple:
            variables = (variables,)
        self.variables = variables

    def __call__(self):
        sentence_results = self.sentence()
        if sentence_results is None:
            import pdb
            pdb.set_trace()
        results = SyntaxBackend.F_Forall(sentence_results)
        return results


class Exists(object):

    def __init__(self, variables, sentence):
        self.sentence = sentence
        if type(variables) is not tuple:
            variables = (variables,)
        self.variables = variables

    def __call__(self):
        sentence_results = self.sentence()
        results = SyntaxBackend.F_Exists(sentence_results)
        return results


