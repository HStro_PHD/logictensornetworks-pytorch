import torch


"""
Combine inputs. In this case they are combined into a single vector
"""
def cross_args(args):
    result = args[0]
    for arg in args[1:]:
        result, _ = cross_2args(result, arg)
    result_flat = torch.reshape(result,
                            (torch.prod(torch.tensor(result.shape[:-1])),
                                result.shape[-1])
                            )

    result_args = torch.split(result_flat,
            [arg.shape[-1] for arg in args],1)
    return result, result_args

def cross_2args(X,Y):
    result = torch.cat([X,Y], dim=-1)
    return result,[X,Y]
