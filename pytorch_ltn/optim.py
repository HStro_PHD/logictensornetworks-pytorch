import torch.optim as optim
import torch
import torch.nn.functional as F


class OptimisationTask(object):

    def __init__(self, constraints, predicates, optim_type='adam', params=None):
        assert type(predicates) in [list, tuple]
        assert type(constraints) in [list, tuple]
        self.predicates = predicates
        self.constraints = constraints
        if optim_type == 'adam':
            if 'weight_decay' not in params:
                params['weight_decay'] = 0
            self.optim = optim.Adam(predicates, lr=params['lr'],
                    weight_decay=params['weight_decay'])
        else:
            raise ValueError('{} type optimiser not supported'.format(optim_type))

    def step(self, retain_graph=False, debug=False):
        self.optim.zero_grad()
        # Cross Entropy Loss
        #sat_list = torch.cat([c() for c in self.constraints]).unsqueeze(-1)
        #loss = -sum([x.log() for x in sat_list])
        #loss = -1/(torch.mean(torch.cat([1/c() for c in self.constraints])))\
        #with torch.autograd.profiler.profile(use_cuda=True) as prof:
        loss = -torch.mean(torch.cat([c() for c in self.constraints]))
        loss.backward(retain_graph=retain_graph)
        #print(prof)
        if debug:
            print('Loss is ', loss.item())
            #print('Groundings', self.constraints[0]())
            for p in self.constraints[0].sentence.predicate.parameters():
                print('Tensor Shape:\t{}\t---\tGrad sum:\t{}'.format(
                    p.grad.shape, p.grad.sum().item())
                    )
                #if len(p.grad.shape) == 3:
                    #print(p)
        self.optim.step()
        return loss.item()

