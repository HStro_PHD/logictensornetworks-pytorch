# TODO I think Michael incorpoarates bias rows everywhere. I don't

import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F

import pytorch_ltn.config
from pytorch_ltn.nn_helper import CustomParameterList
from pytorch_ltn.utils import cross_args

class Predicate(object):

    def __init__(self, label, num_features, arity, pred_def='ntn', params=None):
        self.name = label
        self.pred = PredicateModule(label, num_features, arity, pred_def, params)

    def __call__(self, *variables):
        """
        This creates an instance of the predicate which points at a variable
        and allows the predicate to later just be called, which will cause it to
        ground all data in its variable.
        """
        pred_inst_class = self.__class__.PredicateInstance(self.name, self.pred, variables)
        return pred_inst_class

    class PredicateInstance(object):

        def __init__(self, label, predicate, variables):
            self.name = label
            self.predicate = predicate
            self.variables = variables

        def __call__(self):
            # unpack variables
            var_data = [var.data for var in self.variables]
            crossed_args, _ = cross_args(var_data)
            return self.predicate(crossed_args)



class PredicateModule(nn.Module):

    def __init__(self, label, num_features, arity, pred_def='ntn', params=None):
        super(PredicateModule, self).__init__()
        self.name = label
        self.arity = arity

        if pred_def == 'ntn':
            if not 'asym_mode' in params:
                params['asym_mode'] = False
            self.pred_def = NTN(num_features, arity, params['k_depth'], label, params['asym_mode'])
        elif pred_def == 'analogy':
            self.pred_def = ANALOGY(num_features, arity, params['k_depth'], label)
        elif pred_def == 'linear':
            self.pred_def = LinearPredicate(num_features, arity, label)
        elif pred_def == 'subtract':
            self.pred_def = SubtractionPredicate(num_features, arity, label)
        else:
            raise ValueError('"{}" predicate type not supported'.format(pred_def))

        #modules = [
        #        param[1] for param in self.pred_def.parameters
        #        ]
        #names = [
        #        param[0] for param in self.pred_def.parameters
        #        ]

        #self.parameters = CustomParameterList(modules, names)

    def forward(self, x):
        return self.pred_def.compute(x)


class Function(object):

    def __init__(self, label, num_features=None, arity=None, data=None, function=None, fun_def=None, params=None):
        self.name = label
        self.arity = arity

        if function is not None:
            self.fun_def = function

        if fun_def is not None:
            if fun_def == 'linear':
                assert arity is not None
                assert num_features is not None
                self.fun_def = Linear(num_features, arity)
            else:
                raise ValueError('"{}" function type not supported'.format(fun_def))

    def __call__(self, *variables):
        return self.__class__.FunctionInstance(self.name, self.fun_def, variables)

    class FunctionInstance(object):

        def __init__(self, label, function, variables):
            self.name = label
            self.function = function
            self.variables = variables

        def __call__(self):
            import pdb
            pdb.set_trace()
            # unpack variables
            var_data = [var.data for var in self.variables]
            crossed_args, _ = cross_args(var_data)
            return self.function(crossed_args)


#################
# Back end pieces
#################
class BilinearPositiveDef(nn.Bilinear):
    """
    A modification of the standard bilinear layer, which ensures that the slices
    of the 3d tensor used in computing the bilinear form are positive definite.
    """

    def __init__(self, *args, **kwargs):
        super(BilinearPositiveDef, self).__init__(*args, **kwargs)
        bilinear_r = 1./(np.sqrt(len(self.weight)**2))
        self.weight.data.uniform_(-bilinear_r, bilinear_r)


    def forward(self, input1, input2):
        w = self.weight
        w_T = w.permute([0, 2, 1])
        pos_def_tensor = torch.matmul(w_T, w)
        return F.bilinear(input1, input2, pos_def_tensor, self.bias)

#### Predicate Definitions ####

class ANALOGY(nn.Module):

    def __init__(self, grounding_dim, arity, k_depth, label):
        super(ANALOGY, self).__init__()
        stacked_grounding_dim = grounding_dim * arity

        self.bilinear = BilinearPositiveDef(
                stacked_grounding_dim,
                stacked_grounding_dim,
                1,
                bias=True
                )
        r = 1. / np.sqrt(stacked_grounding_dim**2)
        self.bilinear.weight.data.uniform_(-r, r)
        """
        self.W = nn.Parameter(
                torch.empty(
                    (stacked_grounding_dim, stacked_grounding_dim, k_depth),
                    dtype=torch.float32)
                    #k_depth, stacked_grounding_dim, stacked_grounding_dim)
                )
        """

    def compute(self, input_):
        import pdb
        pdb.set_trace()
        x = self.bilinear(input_, input_)
        #x = torch.einsum('bi,ijk,bj->bk',
                #(input_, self.W, input_))
        #x = x.sum(1).unsqueeze(1)
        return F.sigmoid(x)


class NTN(nn.Module):
    """Predicate computation object, as in NTN
    Each call of 'compute' takes in input embeddings vectors and computes the,
    in this implementation, NTN equation.
    """
    def __init__(self, grounding_dim, arity, k_depth, label, asym_mode):
        """
        Args:
            arity: predicates arity, used to assert correct number of arguments
                during computation
        """
        """
        Note:
            - we are using stacked_grounding_dim as, contrary to NTN, LTN
            paper stacks the predicate arguments such that the same stacked
            vector is on either side of the W matrix. The V operation is unchanged
        """
        super(NTN, self).__init__()
        self.label = label
        self.asym_mode = asym_mode
        stacked_grounding_dim = grounding_dim * arity
        if self.asym_mode and arity == 2:
            W_dim = int(stacked_grounding_dim / 2)
        elif self.asym_mode and arity != 2:
            raise ValueError('Invalid arity of {} for asym_mode NTN'.format(arity))
        else:
            W_dim = stacked_grounding_dim

        self.V = nn.Parameter(
                #torch.tensor(k_depth, stacked_grounding_dim))
                torch.empty(
                    (stacked_grounding_dim, k_depth),
                    dtype=torch.float32)
                )

        self.W = nn.Parameter(
                torch.empty(
                    (W_dim, W_dim, k_depth),
                    dtype=torch.float32)
                    #k_depth, stacked_grounding_dim, stacked_grounding_dim)
                )
        #self.Bilinear = BilinearPositiveDef(
        #        stacked_grounding_dim,
        #        stacked_grounding_dim,
        #        k_depth,
        #        bias=False
        #        )

        self.B = nn.Parameter(
                torch.empty(
                    (k_depth),
                    dtype=torch.float32)
                )

        self.U = nn.Parameter(
                torch.empty(
                    (k_depth, 1),
                    dtype=torch.float32)
                )

        # TODO don't think I need to do this
        self.tanh = nn.Tanh()
        self.sigmoid = nn.Sigmoid()
        self.reset_params()
        #bilinear_params = (param for param in self.Bilinear.parameters())
        #self.parameters = [
        #        (label+'_W', self.W),
                #(label+'_W', bilinear_params),
        #        (label+'_V', self.V),
        #        (label+'_B', self.B),
        #        (label+'_U', self.U)
        #        ]

    def reset_params(self):
        scl = 1e10
        r = 1./np.sqrt(scl*self.W.shape[0])
        w_r = 1./np.sqrt(scl*self.W.shape[0])
        #self.W.data.uniform_(-w_r, w_r)#.normal_()a
        self.W.data.normal_(std=w_r)#.uniform_(-r, r)
        v_r = 1./np.sqrt(scl*self.V.shape[0])
        #self.V.data.uniform_(-v_r, v_r)#.normal_()
        self.V.data.normal_(std=v_r)#.uniform_(-r, r)
        u_r = 1./np.sqrt(scl*self.U.shape[0])
        #self.U.data.uniform_(-u_r, u_r)
        self.U.data.normal_(std=u_r)#.uniform_(-r, r)
        b_r = 1./np.sqrt(scl*self.B.shape[0])
        #self.B.data.uniform_(-b_r, b_r)a
        self.B.data.normal_(std=b_r)#.uniform_(-r, r)


    def compute(self, stacked_inputs):
        """
        Compute predicate grounding for input_
        Args:
            input_: column vector, with each arg stacked. This is how LTNs
            perform the predicate grounding, whereas NTN would have one argument
            on each side of the W matrix in a e1, R, e2 triplet. LTNs however
            open up to arbitrary numbers of arguments since all of their
            embeddings.
        """
        #t0 = time.perf_counter()
        #t0_xwx = time.perf_counter()
        #batch_h = self.Bilinear(stacked_inputs, stacked_inputs)


        if self.asym_mode:
            batch_h = torch.einsum('bi, ijk, bj -> bk',
                    stacked_inputs[:, :, 0], self.W, stacked_inputs[:, :, 1])
            stacked_inputs = stacked_inputs.view(stacked_inputs.size(0), -1)  # turn to column vector
        else:
            batch_h = torch.einsum('bi,ijk,bj->bk',
                    (stacked_inputs, self.W, stacked_inputs))

        #t1_xwx = time.perf_counter()
        mx_plus_b = torch.matmul(  # Can just use nn.Linear and use its bias..
                stacked_inputs, self.V) + self.B  # Broadcast on self.B
        #t1_mxplusb = time.perf_counter()
        non_linear = self.tanh(batch_h + mx_plus_b)
        #t1_non_linear = time.perf_counter()
        output = torch.matmul(non_linear, self.U)
        boolean_output = self.sigmoid(output)
        #t1 = time.perf_counter()
        return boolean_output

class LinearPredicate(nn.Module):
    """n x mn matrix * mn vector  + n vector"""

    def __init__(self, num_features, arity, label):
        super(LinearPredicate, self).__init__()
        self.label = label
        self.arity = arity

        self.M = nn.Linear(num_features*arity, 1)

        self.N = nn.Parameter(
                torch.empty(1)
                )
        self.N.data.normal_()

    def compute(self, input_):
        return self.forward(input_)

    def forward(self, input_):
        return torch.sigmoid(self.M(input_))

class SubtractionPredicate(nn.Module):
    """\sigmoid( A^T (x_1 - x_2) )"""

    def __init__(self, num_features, arity, label):
        super(SubtractionPredicate, self).__init__()
        self.label = label
        self.arity = arity

        try:
            assert arity == 2
        except AssertionError:
            raise ValueError('Subtraction predicate is suitable for binary arguments only!')

        self.A = nn.Parameter(
                torch.empty(num_features, 1)
                )
        scl = 1e10
        a_r = 1./np.sqrt(scl*self.A.shape[0])
        #self.U.data.uniform_(-u_r, u_r)
        self.A.data.normal_(std=a_r)

    def compute(self, input_):
        return self.forward(input_)

    def forward(self, input_):
        mid_point = input_.shape[1] // 2
        return torch.sigmoid(torch.matmul(self.A.t(), (input_[:, :mid_point] - input_[:, mid_point:]).t()))



#### Function Definitions ####

class Linear(nn.Module):
    """n x mn matrix * mn vector  + n vector"""

    def __init__(self, num_features, arity, label):
        super(Linear, self).__init__()
        self.label = label
        self.arity = arity

        self.M = nn.Parameter(
                torch.empty(num_features, num_features*arity)
                )
        self.M.data.normal_()

        self.N = nn.Parameter(
                torch.empty(num_features)
                )
        self.N.data.normal_()

    def forward(self, input_):
        """
        Args:
            input_: column vector stacked for each arg of function
        """
        stacked_input = input_
        MX = torch.matmul(self.M, stacked_input)
        MX_plus_N = MX + self.N
        return MX_plus_N

    def compute(self, input_):
        return self.forward(input_)

# Not sure how this is used in Michaels code. Broken but maybe not worth fixing
# Should it be a class? In principle it's a simple variable... How is input passed?
class Proposition(object):

    def __init__(self, value=None, isFixed=False, label=''):
        self.name = label
        if value is not None:
            assert 0 <= value <= 1
            self.P = torch.FloatTensor([value], requires_grad=isFixed)
        else:
            self.P = torch.empty((1, 1), dtype=torch.float32, requires_grad=isFixed)
            self.P = torch.clamp(
                    self.P.normal_(mean=.5, std=.5), min=0., max=1.)


            def forward(self, x):
                return self.P

#### Data Management Pieces ####

class Variable(object):
    """
    This class acts as a logic variable with an associated data set.

    data can be a torch tensor or a numpy matrix. Rows must be instances
    """
    def __init__(self, label='', num_features=None, data=None, dtype=torch.float32):
        if data is None:
            # Placeholder case - data filled later
            assert num_features is not None
            self.num_features = num_features
            self.data = None
            self.shape = [-1, self.num_features]
        else:
            assert isinstance(data, torch.Tensor) or isinstance(data, np.ndarray)
            self.num_features = data.shape[1:] # Rows are data instances
            if isinstance(data, torch.Tensor):
                self.data = data.type(dtype)
            else:
                self.data = torch.as_tensor(data, dtype=dtype)
            self.shape = self.data.shape

        self.name = label

    def filldata(self, data, dtype=None):
        if dtype is None:
            # default dtype
            dtype = torch.float32
        assert isinstance(data, torch.Tensor) or isinstance(data, np.ndarray)
        self.num_features = data.shape[1:] # Rows are data instances
        if isinstance(data, torch.Tensor):
            self.data = data.type(dtype)
        else:
            self.data = torch.as_tensor(data, dtype=dtype)
        self.shape = self.data.shape

    def get_subset(self, pcnt):
        assert pcnt <= 1.
        num_to_return = int(self.data * pcnt)
        return np.random.choice(self.data, num_to_return)




class Constant(object):

    def __init__(self, label='', value=None, min_value=None, max_value=None):
        if label != '':
            label = 'ltn_constant_'+label
        else:
            label = 'ltn_constant'
        if value is not None:
            self.data = torch.tensor(value, dtype=torch.float32, requires_grad=False)
        else:
            self.data = torch.empty(
                    1, len(min_value),
                    dtype=torch.float32
                    ).uniform_(min_value, max_value)
            self.shape = self.data.shape
        self.name = label


