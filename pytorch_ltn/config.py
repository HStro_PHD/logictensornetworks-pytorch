import torch

use_cuda = torch.cuda.is_available()
if use_cuda:
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
DEVICE = torch.device('cuda' if use_cuda else 'cpu')



